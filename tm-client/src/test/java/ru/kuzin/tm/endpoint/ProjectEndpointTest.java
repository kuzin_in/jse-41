package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.endpoint.IProjectEndpoint;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.request.*;
import ru.kuzin.tm.dto.response.*;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.marker.SoapCategory;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String token;

    @Nullable
    private ProjectDTO project;

    @Before
    public void initTest() throws Exception {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        token = userLoginResponse.getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 1");
        projectCreateRequest.setDescription("Project Description 1");
        @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                projectCreateRequest
        );
        project = projectCreateResponse.getProject();
    }

    @After
    public void initEndTest() throws Exception {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        Assert.assertNotNull(project);
        @Nullable final String id = project.getId();
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final ProjectChangeStatusByIdRequest projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setStatus(null);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setId(id);
        projectChangeStatusByIdRequest.setStatus(newStatus);
        @NotNull final ProjectChangeStatusByIdResponse projectChangeStatusByIdResponse = projectEndpoint.changeProjectStatusById(
                projectChangeStatusByIdRequest
        );
        @Nullable final ProjectDTO actualProject = projectChangeStatusByIdResponse.getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() throws Exception {
        Assert.assertNotNull(project);
        @NotNull final Integer projectIndex = 0;
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final ProjectChangeStatusByIndexRequest projectChangeStatusByIndexRequest = new ProjectChangeStatusByIndexRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest)
        );
        projectChangeStatusByIndexRequest.setIndex(485);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest)
        );
        projectChangeStatusByIndexRequest.setStatus(null);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(projectChangeStatusByIndexRequest)
        );
        projectChangeStatusByIndexRequest.setIndex(projectIndex);
        projectChangeStatusByIndexRequest.setStatus(newStatus);
        @NotNull final ProjectChangeStatusByIndexResponse projectChangeStatusByIndexResponse = projectEndpoint.changeProjectStatusByIndex(
                projectChangeStatusByIndexRequest
        );
        @Nullable final ProjectDTO actualProject = projectChangeStatusByIndexResponse.getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
    }

    @Test
    public void testClearProject() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        Assert.assertNotNull(projectEndpoint.createProject(projectCreateRequest));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token);
        @Nullable List<ProjectDTO> projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);

        Assert.assertNotNull(projectEndpoint.clearProject(new ProjectClearRequest(token)));
        @NotNull final ProjectListResponse actualListResponse = projectEndpoint.listProject(listRequest);
        projects = actualListResponse.getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testCreateProject() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(projectCreateRequest)
        );
        projectCreateRequest.setName("");
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(projectCreateRequest)
        );
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                projectCreateRequest
        );
        Assert.assertNotNull(projectCreateResponse);
        @Nullable ProjectDTO actualProject = projectCreateResponse.getProject();
        Assert.assertNotNull(actualProject);
    }

    @Test
    public void testShowProjectById() throws Exception {
        Assert.assertNotNull(project);
        @NotNull String id = project.getId();
        @NotNull final ProjectShowByIdRequest projectShowByIdRequest = new ProjectShowByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.showProjectById(projectShowByIdRequest)
        );
        projectShowByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.showProjectById(projectShowByIdRequest)
        );
        projectShowByIdRequest.setId(id);
        @NotNull final ProjectShowByIdResponse projectShowByIdResponse = projectEndpoint.showProjectById(projectShowByIdRequest);
        @Nullable final ProjectDTO actualProject = projectShowByIdResponse.getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals("Project 1", actualProject.getName());
    }

    @Test
    public void testShowProjectList() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        Assert.assertNotNull(projectEndpoint.createProject(projectCreateRequest));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token);
        @Nullable List<ProjectDTO> projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token);
        @Nullable List<ProjectDTO> projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
        @NotNull final String id = projects.get(0).getId();
        @NotNull final ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(projectRemoveByIdRequest)
        );
        projectRemoveByIdRequest.setId(id);
        Assert.assertNotNull(projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testUpdateProjectById() throws Exception {
        Assert.assertNotNull(project);
        @Nullable String id = project.getId();

        @NotNull final ProjectUpdateByIdRequest projectUpdateByIdRequest = new ProjectUpdateByIdRequest(token);
        projectUpdateByIdRequest.setId(id);
        projectUpdateByIdRequest.setName("New Project Name");
        projectUpdateByIdRequest.setDescription("New Description");
        @NotNull final ProjectUpdateByIdResponse projectUpdateByIdResponse = projectEndpoint.updateProjectById(
                projectUpdateByIdRequest
        );
        @Nullable final ProjectDTO actualProject = projectUpdateByIdResponse.getProject();
        Assert.assertNotNull(actualProject);
        Assert.assertEquals("New Project Name", actualProject.getName());
        Assert.assertEquals("New Description", actualProject.getDescription());

        projectUpdateByIdRequest.setName("");
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
        projectUpdateByIdRequest.setName(null);
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
        projectUpdateByIdRequest.setId("otherId");
        projectUpdateByIdRequest.setName("New Project Name");
        Assert.assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
    }

}