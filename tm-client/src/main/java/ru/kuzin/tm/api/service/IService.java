package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.repository.IRepository;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort);

}