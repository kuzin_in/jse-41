package ru.kuzin.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.service.IServiceLocator;
import ru.kuzin.tm.dto.request.AbstractUserRequest;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.exception.user.AccessDeniedException;
import ru.kuzin.tm.dto.model.SessionDTO;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}